﻿using Teste.BureauVeritas.Domain.Contracts.AppServices.Authentication;
using Teste.BureauVeritas.Domain.Contracts.Services.Authentication;
using Teste.BureauVeritas.Domain.Util.Autenticacao;

namespace Teste.BureauVeritas.AppService.AppServices.Authentication
{
    public class AutenticacaoAppService : IAutenticacaoAppService
    {
        private readonly IAutenticacaoService _service;

        public AutenticacaoAppService(IAutenticacaoService service)
        {
            _service = service;
        }

        public Autenticacao Autenticar(string url, string method)
        {
            return _service.Autenticar(url, method);
        }
    }
}