﻿using Teste.BureauVeritas.AppService.AppServices.Base;
using Teste.BureauVeritas.Domain.Contracts.AppServices;
using Teste.BureauVeritas.Domain.Contracts.Services;
using Teste.BureauVeritas.Domain.Models;

namespace Teste.BureauVeritas.AppService.AppServices
{
    public class CategoriaAppService : BaseAppService<Categoria>, ICategoriaAppService
    {
        private readonly ICategoriaService _categoriaService;

        public CategoriaAppService(ICategoriaService categoriaService) : base(categoriaService)
        {
            _categoriaService = categoriaService;
        }
    }
}