﻿using System.Collections.Generic;
using Teste.BureauVeritas.AppService.AppServices.Base;
using Teste.BureauVeritas.Domain.Contracts.AppServices;
using Teste.BureauVeritas.Domain.Contracts.Services;
using Teste.BureauVeritas.Domain.DTOs;
using Teste.BureauVeritas.Domain.Models;

namespace Teste.BureauVeritas.AppService.AppServices
{
    public class ProdutoAppService : BaseAppService<Produto>, IProdutoAppService
    {
        private readonly IProdutoService _produtoService;

        public ProdutoAppService(IProdutoService produtoService) : base(produtoService)
        {
            _produtoService = produtoService;
        }

        public IEnumerable<ProdutoDTO> ObterTodosComCategorias()
        {
            return _produtoService.ObterTodosComCategorias();
        }

        public IEnumerable<ProdutoDTO> ObterTodosViaAPI(string url, string method, string token)
        {
            return _produtoService.ObterTodosViaAPI(url, method, token);
        }
    }
}