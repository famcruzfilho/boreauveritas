﻿using Newtonsoft.Json;
using Teste.BureauVeritas.Business.Util;
using Teste.BureauVeritas.Domain.Contracts.Services.Authentication;
using Teste.BureauVeritas.Domain.Util.Autenticacao;

namespace Teste.BureauVeritas.Business.Services.Authentication
{
    public class AutenticacaoService : IAutenticacaoService
    {
        public Autenticacao Autenticar(string url, string method)
        {
            var json = ConsultaAPI.ExecutarConsulta(url, method, null);
            return JsonConvert.DeserializeObject<Autenticacao>(json);
        }
    }
}