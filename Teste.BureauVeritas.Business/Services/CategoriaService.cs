﻿using Teste.BureauVeritas.Business.Services.Base;
using Teste.BureauVeritas.Domain.Contracts.Repositories;
using Teste.BureauVeritas.Domain.Contracts.Services;
using Teste.BureauVeritas.Domain.Models;

namespace Teste.BureauVeritas.Business.Services
{
    public class CategoriaService : BaseService<Categoria>, ICategoriaService
    {
        private readonly ICategoriaRepository _categoriaRepository;

        public CategoriaService(ICategoriaRepository categoriaRepository) : base(categoriaRepository)
        {
            _categoriaRepository = categoriaRepository;
        }
    }
}