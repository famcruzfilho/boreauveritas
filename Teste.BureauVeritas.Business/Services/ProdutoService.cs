﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Teste.BureauVeritas.Business.Services.Base;
using Teste.BureauVeritas.Business.Util;
using Teste.BureauVeritas.Domain.Contracts.Repositories;
using Teste.BureauVeritas.Domain.Contracts.Services;
using Teste.BureauVeritas.Domain.DTOs;
using Teste.BureauVeritas.Domain.Models;

namespace Teste.BureauVeritas.Business.Services
{
    public class ProdutoService : BaseService<Produto>, IProdutoService
    {
        private readonly IProdutoRepository _produtoRepository;

        public ProdutoService(IProdutoRepository produtoRepository) : base(produtoRepository)
        {
            _produtoRepository = produtoRepository;
        }

        public IEnumerable<ProdutoDTO> ObterTodosComCategorias()
        {
            return _produtoRepository.ObterTodosComCategorias();
        }

        public IEnumerable<ProdutoDTO> ObterTodosViaAPI(string url, string method, string token)
        {
            var json = ConsultaAPI.ExecutarConsulta(url, method, token);
            return JsonConvert.DeserializeObject<IEnumerable<ProdutoDTO>>(json);
        }
    }
}