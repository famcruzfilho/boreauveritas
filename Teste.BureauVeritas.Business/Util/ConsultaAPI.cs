﻿using System;
using System.IO;
using System.Net;

namespace Teste.BureauVeritas.Business.Util
{
    public static class ConsultaAPI
    {
        public static string ExecutarConsulta(string url, string method, string token)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                string data = string.Empty;
                WebRequest request = WebRequest.Create(url);
                request.Method = method;
                if (!string.IsNullOrEmpty(token))
                {
                    request.Headers.Add("Authorization", "Bearer " + token);
                }
                WebResponse response = request.GetResponse();
                using (Stream dataStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(dataStream);
                    string responseFromServer = reader.ReadToEnd();
                    data = responseFromServer;
                }
                response.Close();
                return data;
            }
            catch
            {
                throw new Exception("Erro de autenticação");
            }
        }
    }
}