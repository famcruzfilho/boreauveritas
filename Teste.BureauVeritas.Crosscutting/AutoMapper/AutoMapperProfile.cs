﻿using AutoMapper;
using Teste.BureauVeritas.Domain.DTOs;
using Teste.BureauVeritas.Domain.Models;
using Teste.BureauVeritas.Domain.ViewModels;

namespace Teste.BureauVeritas.Crosscutting.AutoMapper
{
    /// <summary>
    /// AutoMapperProfile
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        /// <summary>
        /// Construtor do AutoMapperProfile
        /// </summary>
        public AutoMapperProfile()
        {
            AutoMapperFilters();
            AutoMapperEntities();
            AutoMapperEntityToModel();
            AutoMapperCustom();
        }

        /// <summary>
        /// AutoMapperFilters
        /// </summary>
        private void AutoMapperFilters()
        {
            //Models para view models and vice versa
            CreateMap<Categoria, CategoriaViewModel>().ReverseMap();
            CreateMap<Produto, ProdutoViewModel>().ReverseMap();

            //Models para DTO's and vice versa
            CreateMap<Categoria, CategoriaDTO>().ReverseMap();
            CreateMap<Produto, ProdutoDTO>().ReverseMap();

            //Models para DTO's and vice versa
            CreateMap<CategoriaDTO, CategoriaViewModel>().ReverseMap();
            CreateMap<ProdutoDTO, ProdutoViewModel>().ReverseMap();
        }

        /// <summary>
        /// AutoMapperEntities
        /// </summary>
        private void AutoMapperEntities()
        {

        }

        /// <summary>
        /// AutoMapperEntityToModel
        /// </summary>
        private void AutoMapperEntityToModel()
        {

        }

        /// <summary>
        /// AutoMapperCustom
        /// </summary>
        private void AutoMapperCustom()
        {

        }
    }
}