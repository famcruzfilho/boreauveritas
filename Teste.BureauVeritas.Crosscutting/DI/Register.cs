﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Teste.BureauVeritas.AppService.AppServices;
using Teste.BureauVeritas.AppService.AppServices.Authentication;
using Teste.BureauVeritas.Business.Services;
using Teste.BureauVeritas.Business.Services.Authentication;
using Teste.BureauVeritas.Domain.Contracts.AppServices;
using Teste.BureauVeritas.Domain.Contracts.AppServices.Authentication;
using Teste.BureauVeritas.Domain.Contracts.Repositories;
using Teste.BureauVeritas.Domain.Contracts.Repositories.UnityOfWork;
using Teste.BureauVeritas.Domain.Contracts.Services;
using Teste.BureauVeritas.Domain.Contracts.Services.Authentication;
using Teste.BureauVeritas.Infraestructure.Data.Context;
using Teste.BureauVeritas.Infraestructure.Data.Repositories;
using Teste.BureauVeritas.Infraestructure.Data.Repositories.UnityOfWork;

namespace Teste.BureauVeritas.Crosscutting.DI
{
    public class Register
    {
        public IServiceCollection RegisterClass(IServiceCollection services, string stringConexao)
        {
            #region [ Repositories ]

            services.AddTransient<ICategoriaRepository, CategoriaRepository>();
            services.AddTransient<IProdutoRepository, ProdutoRepository>();

            #endregion

            #region [ Unity Of Work ]

            services.AddTransient<IUnityOfWork, UnityOfWork>();

            #endregion

            #region [ Services ]

            services.AddTransient<ICategoriaService, CategoriaService>();
            services.AddTransient<IProdutoService, ProdutoService>();

            #endregion

            #region [ Application Services ]

            services.AddTransient<ICategoriaAppService, CategoriaAppService>();
            services.AddTransient<IProdutoAppService, ProdutoAppService>();

            #endregion

            #region [ Authentication ]

            services.AddTransient<IAutenticacaoAppService, AutenticacaoAppService>();
            services.AddTransient<IAutenticacaoService, AutenticacaoService>();

            #endregion

            return services.AddDbContext<DataContext>(options => options.UseSqlServer(stringConexao));
        }
    }
}