﻿using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography;

namespace Teste.BureauVeritas.Crosscutting.Secutiry
{
    /// <summary>
	/// Class to represent SigningConfiguration
	/// </summary>
    public class SigningConfiguration
    {
        #region [ Properties ]
        /// <summary>
        /// Property Key
        /// </summary>
        public SecurityKey Key { get; }
        /// <summary>
        /// Property Credentials
        /// </summary>
        public SigningCredentials Credentials { get; }
        #endregion

        #region [ Constructor ]

        /// <summary>
        /// Constructor to initialize Key and Credentials
        /// </summary>
        public SigningConfiguration()
        {
            using (var provider = new RSACryptoServiceProvider(2048))
            {
                Key = new RsaSecurityKey(provider.ExportParameters(true));
            }

            Credentials = new SigningCredentials(Key, SecurityAlgorithms.RsaSha256Signature);
        }

        #endregion
    }
}