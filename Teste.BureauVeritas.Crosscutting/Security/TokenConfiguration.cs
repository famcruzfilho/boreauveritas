﻿namespace Teste.BureauVeritas.Crosscutting.Secutiry
{
    /// <summary>
	/// Class to represent TokenConfiguration from appsettings.json
	/// </summary>
    public class TokenConfiguration
    {
        #region [ Properties ]
        /// <summary>
        /// Property Audience
        /// </summary>
        public string Audience { get; set; }
        /// <summary>
        /// Property Issuer
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// Property Seconds
        /// </summary>
        public int Seconds { get; set; }
        #endregion
    }
}