﻿using Teste.BureauVeritas.Domain.Util.Autenticacao;

namespace Teste.BureauVeritas.Domain.Contracts.AppServices.Authentication
{
    public interface IAutenticacaoAppService
    {
        Autenticacao Autenticar(string url, string method);
    }
}