﻿using Teste.BureauVeritas.Domain.Contracts.AppServices.Base;
using Teste.BureauVeritas.Domain.Models;

namespace Teste.BureauVeritas.Domain.Contracts.AppServices
{
    public interface ICategoriaAppService : IBaseAppService<Categoria>
    {

    }
}