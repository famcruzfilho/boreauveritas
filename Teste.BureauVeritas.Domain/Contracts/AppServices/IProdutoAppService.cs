﻿using System.Collections.Generic;
using Teste.BureauVeritas.Domain.Contracts.AppServices.Base;
using Teste.BureauVeritas.Domain.DTOs;
using Teste.BureauVeritas.Domain.Models;

namespace Teste.BureauVeritas.Domain.Contracts.AppServices
{
    public interface IProdutoAppService : IBaseAppService<Produto>
    {
        IEnumerable<ProdutoDTO> ObterTodosComCategorias();
        IEnumerable<ProdutoDTO> ObterTodosViaAPI(string url, string method, string token);
    }
}