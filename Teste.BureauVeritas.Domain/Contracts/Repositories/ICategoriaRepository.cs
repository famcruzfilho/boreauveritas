﻿using Teste.BureauVeritas.Domain.Contracts.Repositories.Base;
using Teste.BureauVeritas.Domain.Models;

namespace Teste.BureauVeritas.Domain.Contracts.Repositories
{
    public interface ICategoriaRepository : IBaseRepository<Categoria>
    {

    }
}