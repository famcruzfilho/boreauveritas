﻿using System.Collections.Generic;
using Teste.BureauVeritas.Domain.Contracts.Repositories.Base;
using Teste.BureauVeritas.Domain.DTOs;
using Teste.BureauVeritas.Domain.Models;

namespace Teste.BureauVeritas.Domain.Contracts.Repositories
{
    public interface IProdutoRepository : IBaseRepository<Produto>
    {
        IEnumerable<ProdutoDTO> ObterTodosComCategorias();
    }
}