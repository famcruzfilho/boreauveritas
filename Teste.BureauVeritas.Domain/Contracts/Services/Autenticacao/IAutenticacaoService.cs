﻿using Teste.BureauVeritas.Domain.Util.Autenticacao;

namespace Teste.BureauVeritas.Domain.Contracts.Services.Authentication
{
    public interface IAutenticacaoService
    {
        Autenticacao Autenticar(string url, string method);
    }
}