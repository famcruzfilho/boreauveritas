﻿using Teste.BureauVeritas.Domain.Contracts.Services.Base;
using Teste.BureauVeritas.Domain.Models;

namespace Teste.BureauVeritas.Domain.Contracts.Services
{
    public interface ICategoriaService : IBaseService<Categoria>
    {

    }
}