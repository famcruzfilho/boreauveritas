﻿using System.Collections.Generic;
using Teste.BureauVeritas.Domain.Contracts.Services.Base;
using Teste.BureauVeritas.Domain.DTOs;
using Teste.BureauVeritas.Domain.Models;

namespace Teste.BureauVeritas.Domain.Contracts.Services
{
    public interface IProdutoService : IBaseService<Produto>
    {
        IEnumerable<ProdutoDTO> ObterTodosComCategorias();
        IEnumerable<ProdutoDTO> ObterTodosViaAPI(string url, string method, string token);
    }
}