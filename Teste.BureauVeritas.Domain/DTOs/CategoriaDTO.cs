﻿using System;
using System.Runtime.Serialization;

namespace Teste.BureauVeritas.Domain.DTOs
{
    [Serializable]
    public class CategoriaDTO : ISerializable
    {
        public int CategoriaId { get; set; }
        public string Nome { get; set; }

        public CategoriaDTO()
        {

        }

        public CategoriaDTO(SerializationInfo info, StreamingContext context)
        {
            CategoriaId = info.GetInt32("categoriaId");
            Nome = info.GetString("nome");
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("categoriaId", CategoriaId);
            info.AddValue("nome", Nome);
        }
    }
}