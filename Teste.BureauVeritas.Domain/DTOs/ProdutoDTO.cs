﻿using System;
using System.Runtime.Serialization;

namespace Teste.BureauVeritas.Domain.DTOs
{
    [Serializable]
    public class ProdutoDTO : ISerializable
    {
        public int ProdutoId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public int CategoriaId { get; set; }
        public CategoriaDTO Categoria { get; set; }
        public double Preco { get; set; }

        public ProdutoDTO()
        {

        }

        public ProdutoDTO(SerializationInfo info, StreamingContext context)
        {
            ProdutoId = info.GetInt32("produtoId");
            Nome = info.GetString("nome");
            Descricao = info.GetString("descricao");
            CategoriaId = info.GetInt32("categoriaId");
            Categoria = (CategoriaDTO)info.GetValue("categoria", typeof(CategoriaDTO));
            Preco = info.GetDouble("preco");
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("produtoId", ProdutoId);
            info.AddValue("nome", Nome);
            info.AddValue("descricao", Descricao);
            info.AddValue("categoriaId", CategoriaId);
            info.AddValue("categoria", Categoria);
            info.AddValue("preco", Preco);
        }
    }
}