﻿namespace Teste.BureauVeritas.Domain.Models
{
    public class Categoria
    {
        public int CategoriaId { get; set; }
        public string Nome { get; set; }
    }
}