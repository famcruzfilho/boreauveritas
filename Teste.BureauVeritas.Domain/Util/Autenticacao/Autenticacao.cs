﻿using System;
using System.Runtime.Serialization;

namespace Teste.BureauVeritas.Domain.Util.Autenticacao
{
    [Serializable]
    public class Autenticacao : ISerializable
    {
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
        public string ExpirationInMinutes { get; set; }

        public Autenticacao()
        {

        }

        public Autenticacao(SerializationInfo info, StreamingContext context)
        {
            Token = info.GetString("token");
            Expiration = info.GetDateTime("expiration");
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("token", Token);
            info.AddValue("expiration", Expiration);
            info.AddValue("expirationInMinutes", ExpirationInMinutes);
        }
    }
}