﻿namespace Teste.BureauVeritas.Domain.ViewModels
{
    public class ProdutoViewModel
    {
        public int ProdutoId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public int CategoriaId { get; set; }
        public CategoriaViewModel Categoria { get; set; }
        public double Preco { get; set; }
    }
}