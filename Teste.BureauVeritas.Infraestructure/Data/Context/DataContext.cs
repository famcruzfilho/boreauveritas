﻿using Microsoft.EntityFrameworkCore;
using Teste.BureauVeritas.Domain.Models;
using Teste.BureauVeritas.Infraestructure.Data.Mappings;

namespace Teste.BureauVeritas.Infraestructure.Data.Context
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoriaMap());
            modelBuilder.ApplyConfiguration(new ProdutoMap());
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        //{
        //    if (!optionBuilder.IsConfigured)
        //    {
        //        optionBuilder.UseSqlServer(connectionString: "Data Source=localhost;Initial Catalog=Teste_BoreauVeritas;User Id=sa;Password=Em0t10n%");
        //    }
        //}
    }
}