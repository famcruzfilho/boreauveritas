﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Teste.BureauVeritas.Domain.Models;

namespace Teste.BureauVeritas.Infraestructure.Data.Mappings
{
    public class CategoriaMap : IEntityTypeConfiguration<Categoria>
    {
        public void Configure(EntityTypeBuilder<Categoria> builder)
        {
            builder.ToTable("Categorias");

            builder.HasKey(x => x.CategoriaId);

            builder.Property(x => x.Nome)
                .IsRequired()
                .HasMaxLength(250);
        }
    }
}