﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Teste.BureauVeritas.Domain.Models;

namespace Teste.BureauVeritas.Infraestructure.Data.Mappings
{
    public class ProdutoMap : IEntityTypeConfiguration<Produto>
    {
        public void Configure(EntityTypeBuilder<Produto> builder)
        {
            builder.ToTable("Produtos");

            builder.HasKey(x => x.ProdutoId);

            builder.Property(x => x.Nome)
                .IsRequired()
                .HasMaxLength(250);

            builder.Property(x => x.Descricao)
                .IsRequired()
                .HasMaxLength(250);

            builder.HasOne(x => x.Categoria);

            builder.Property(x => x.Preco)
                .IsRequired();
        }
    }
}