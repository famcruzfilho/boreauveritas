﻿using Teste.BureauVeritas.Domain.Contracts.Repositories;
using Teste.BureauVeritas.Domain.Models;
using Teste.BureauVeritas.Infraestructure.Data.Context;
using Teste.BureauVeritas.Infraestructure.Data.Repositories.Base;

namespace Teste.BureauVeritas.Infraestructure.Data.Repositories
{
    public class CategoriaRepository : BaseRepository<Categoria>, ICategoriaRepository
    {
        private readonly DataContext _context;

        public CategoriaRepository(DataContext context) : base(context)
        {
            _context = context;
        }
    }
}