﻿using System.Collections.Generic;
using System.Linq;
using Teste.BureauVeritas.Domain.Contracts.Repositories;
using Teste.BureauVeritas.Domain.DTOs;
using Teste.BureauVeritas.Domain.Models;
using Teste.BureauVeritas.Infraestructure.Data.Context;
using Teste.BureauVeritas.Infraestructure.Data.Repositories.Base;

namespace Teste.BureauVeritas.Infraestructure.Data.Repositories
{
    public class ProdutoRepository : BaseRepository<Produto>, IProdutoRepository
    {
        private readonly DataContext _context;

        public ProdutoRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<ProdutoDTO> ObterTodosComCategorias()
        {
            var data = _context.Produtos.Select(x => new ProdutoDTO
            {
                ProdutoId = x.ProdutoId,
                Nome = x.Nome,
                Descricao = x.Descricao,
                CategoriaId = x.CategoriaId,
                Categoria = new CategoriaDTO
                {
                    CategoriaId = x.Categoria.CategoriaId,
                    Nome = x.Categoria.Nome
                },
                Preco = x.Preco
            }).AsQueryable();

            return data.ToList();
        }
    }
}