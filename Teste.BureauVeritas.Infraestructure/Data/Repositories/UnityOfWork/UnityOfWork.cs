﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Threading.Tasks;
using Teste.BureauVeritas.Domain.Contracts.Repositories.UnityOfWork;
using Teste.BureauVeritas.Infraestructure.Data.Context;

namespace Teste.BureauVeritas.Infraestructure.Data.Repositories.UnityOfWork
{
    public class UnityOfWork : IUnityOfWork
    {
        public readonly DataContext _context;
        private IDbContextTransaction _contextTransaction;
        public CategoriaRepository _categoriaContext { get; private set; }
        public ProdutoRepository _produtoContext { get; private set; }

        public UnityOfWork(DataContext context)
        {
            _context = context;
            _categoriaContext = new CategoriaRepository(_context);
            _produtoContext = new ProdutoRepository(_context);
        }

        public async Task Commit()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void BeginTransaction() => _contextTransaction = _context.Database.BeginTransaction();

        public void SaveChanges() => _context.SaveChanges();

        public void Rollback() => _contextTransaction.Rollback();
    }
}