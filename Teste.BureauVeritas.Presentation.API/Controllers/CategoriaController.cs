﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nancy.Json;
using Teste.BureauVeritas.Domain.Contracts.AppServices;

namespace Teste.BureauVeritas.Presentation.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaController : ControllerBase
    {
        private readonly ICategoriaAppService _categoriaAppService;
        private readonly IMapper _mapper;

        public CategoriaController(ICategoriaAppService categoriaAppService, IMapper mapper)
        {
            _categoriaAppService = categoriaAppService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<string> Get()
        {
            return new JavaScriptSerializer().Serialize(_categoriaAppService.ObterTodos());
        }
    }
}