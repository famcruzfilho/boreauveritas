﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nancy.Json;
using Teste.BureauVeritas.Domain.Contracts.AppServices;

namespace Teste.BureauVeritas.Presentation.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private readonly IProdutoAppService _produtoAppService;
        private readonly IMapper _mapper;

        public ProdutoController(IProdutoAppService produtoAppService, IMapper mapper)
        {
            _produtoAppService = produtoAppService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<string> Get()
        {
            return new JavaScriptSerializer().Serialize(_produtoAppService.ObterTodosComCategorias());
        }
    }
}