import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { API_PRODUTOS } from './app.api';

@Injectable()

export class AppService {
    constructor(private http: Http)
    {

    }

    getToken(){
        return new Promise<any>((resolve, reject) => {
            this.http.get(`${API_PRODUTOS}/api/Autenticacao/Login`)
                    .subscribe((data) => {
                        resolve(data.json());
                    }, (error) => { reject(error) });
        })
    }

    getProducts(token: string){
        return new Promise<any>((resolve, reject) => {
            let headers: Headers = new Headers();
            headers.append("Authorization", "Bearer " + token)
            headers.append("Content-Type", "application/json")
            this.http.get(`${API_PRODUTOS}/api/Produto`, { headers: headers })
                    .subscribe((data) => {
                        resolve(data.json());
                    }, (error) => { reject(error) });
        })
    }
}