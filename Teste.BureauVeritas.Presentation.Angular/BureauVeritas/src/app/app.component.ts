import { Component, OnInit } from '@angular/core';
import { AppService } from './app.component.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  token: string
  expirationInMinutes: string
  products: any[] = []

  constructor(private appService: AppService)
  {
    
  }

  ngOnInit() {
  }

  GerarToken(){
    this.appService.getToken().then(token => { 
      this.token = token.token
      this.expirationInMinutes = token.expirationInMinutes
     })
  }

  ListarProdutos(){
    if(this.token)
    {
      this.appService.getProducts(this.token).then(products => { 
        this.products = products;
       })
    }
    else
    {
      this.products = [];
      alert("Requisição não autorizada");
    }
  }
}
