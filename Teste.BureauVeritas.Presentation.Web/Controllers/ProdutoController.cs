﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Teste.BureauVeritas.Domain.Contracts.AppServices;
using Teste.BureauVeritas.Domain.Contracts.AppServices.Authentication;
using Teste.BureauVeritas.Domain.DTOs;
using Teste.BureauVeritas.Domain.Util.Autenticacao;
using Teste.BureauVeritas.Domain.ViewModels;

namespace Teste.BureauVeritas.Presentation.Web.Controllers
{
    public class ProdutoController : Controller
    {
        private readonly IProdutoAppService _produtoAppService;
        private readonly IAutenticacaoAppService _autenticacaoAppService;
        private readonly IMapper _mapper;

        public ProdutoController(IProdutoAppService produtoAppService, IAutenticacaoAppService autenticacaoAppService,
            IMapper mapper)
        {
            _produtoAppService = produtoAppService;
            _autenticacaoAppService = autenticacaoAppService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        public PartialViewResult Input()
        {
            return PartialView("_Input");
        }

        [HttpPost]
        [Route("GerarToken")]
        public JsonResult GerarToken()
        {
            Autenticacao autenticacao = _autenticacaoAppService.Autenticar("https://localhost:44361/api/Autenticacao/Login", "POST");
            return Json(autenticacao);
        }

        [HttpPost]
        [Route("ListarProdutos")]
        public PartialViewResult ListarProdutos(string token)
        {
            return PartialView("_GridProdutos", _mapper.Map<IEnumerable<ProdutoDTO>, IEnumerable<ProdutoViewModel>>(_produtoAppService.ObterTodosViaAPI("https://localhost:44361/api/Produto", "GET", token)));
        }
    }
}