﻿function GerarToken() {
    $.ajax({
        url: urlGerarToken,
        type: 'GET',
        success: function (result) {
            $("#Token").val(result.token);
            $("#Expiration").val(result.expiration);
        }
    });
}

function ListarProdutos() {
    var token = $("#Token").val();
    if (token != "") {
        $.ajax({
            url: urlListarProdutos,
            data: { token: token },
            type: 'POST',
            success: function (result) {
                $("#GridProdutos").append(result);
            }
        });
    }
}